package com.example.sneck

import android.graphics.Canvas
import android.os.Build
import android.view.SurfaceHolder
import androidx.annotation.RequiresApi

class GameLoop(game: Game, holder: SurfaceHolder) : Thread() {
    private val game: Game
    private val holder: SurfaceHolder

    init {
        this.game = game
        this.holder = holder
    }

    fun startLoop() {
        running = true
        this.start()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun run() {
        super.run()
        var canvas: Canvas? = null
        val startTime: Long = System.currentTimeMillis()
        var timestamp: Long
        var timeElapsed: Long
        var updatesCount: Long = 0
        while (running) {
            timestamp = System.currentTimeMillis()
            try {
                if (holder.surface.isValid)
                    canvas = holder.lockCanvas()
            } catch (e: Exception) {
                e.printStackTrace()
            }
            if (canvas != null)
                try {
                    synchronized(holder) {
                        game.update()
                        ++updatesCount
                        game.draw(canvas)
                        //Log.w("geg", "upd_drw")
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    try {
                        if (holder.surface.isValid)
                            holder.unlockCanvasAndPost(canvas)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            timeElapsed = System.currentTimeMillis() - timestamp
            if (timeElapsed < 16) {
                sleep(16 - timeElapsed)
            } else while (System.currentTimeMillis() - startTime > updatesCount * 16) {
                game.update()
                ++updatesCount
                //Log.w("geg", "upd")
            }
        }
    }

    fun stopLoop() {
        running = false
    }


    @Volatile
    var running: Boolean = false
}