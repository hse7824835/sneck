package com.example.sneck

import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Paint
import android.view.SurfaceHolder
import android.view.SurfaceView
import android.graphics.Color
import android.graphics.Rect
import android.graphics.Typeface
import android.os.Build
import android.view.MotionEvent
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat.startActivity
import java.lang.Float.max
import java.lang.Float.min
import kotlin.math.pow

class Game(context: Context, private val moveTime: Int) : SurfaceView(context),
    SurfaceHolder.Callback {
    private val gameLoop: GameLoop
    private val sneck: Sneck
    private val gameField: GameField
    private var touchCoordinate: Coordinate? = null
    private var intendedDirection: Direction? = null
    private var aliveUntil: Long = Long.MAX_VALUE
    private var score: Int = 0
    private var background: Bitmap
    private var gravityDirections: Bitmap

    init {
        holder.addCallback(this)
        isFocusable = true
        gameLoop = GameLoop(this, holder)
        gameField = GameField(context)
        sneck = Sneck(context, gameField)
        val opts = BitmapFactory.Options()
        opts.inScaled = false
        background = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.background,
            opts
        )
        gravityDirections = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.gravity_directions,
            opts
        )
    }


    override fun surfaceCreated(p0: SurfaceHolder) {
        gameLoop.startLoop()
    }

    override fun surfaceChanged(p0: SurfaceHolder, p1: Int, p2: Int, p3: Int) {
    }

    override fun surfaceDestroyed(p0: SurfaceHolder) {
        gameLoop.stopLoop()
        var retry = true
        while (retry) {
            try {
                gameLoop.join()
                retry = false
            } catch (e: InterruptedException) {
                e.printStackTrace()
            }
        }
    }

    fun update() {
        if (System.currentTimeMillis() > aliveUntil) {
            gameLoop.stopLoop()
            val i = Intent(context, DeadActivity::class.java)
            i.putExtra("moveTime", moveTime)
            i.putExtra("score", score)
            startActivity(context, i, null)
        }
        if (intendedDirection != null) {
            sneck.move(intendedDirection!!)
            intendedDirection = null
        }
        if (!gameField.hasFood) {
            aliveUntil = System.currentTimeMillis() + moveTime * 1000
            if (sneck.fallUntil <= System.currentTimeMillis()) {
                ++score
                gameField.spawnFood(sneck)
            }
        }
    }

    //1080 x 2160 = 18*60 x 36*60
    //18*60 x 34*60 :D => 16x32
    @RequiresApi(Build.VERSION_CODES.O)
    override fun draw(canvas: Canvas) {
        super.draw(canvas)
        canvas.drawBitmap(background, Rect(0, 0, 1080, 2160), Rect(0, 0, 1080, 2160), Paint())
        val gravityIndex = when (sneck.gravity) {
            Direction.UP -> 0
            Direction.RIGHT -> 1
            Direction.DOWN -> 2
            Direction.LEFT -> 3
        }
        canvas.drawBitmap(
            gravityDirections,
            Rect(0, 54 * gravityIndex, 810, 54 * gravityIndex + 54),
            Rect(240, 30, 240 + 810, 30 + 54),
            Paint()
        )
        val timeLeft = min(max(0f, (aliveUntil - System.currentTimeMillis()).toFloat() * 0.001f / moveTime), 1f)
        val p = Paint()
        p.color = Color.rgb(1f - timeLeft.pow(4),1f - (1f - timeLeft).pow(4),0f)
        canvas.drawRect(270f, 120f, 270f + 750f*timeLeft, 144f, p)
        p.color = Color.rgb(1f, .5f, .5f)
        p.typeface = resources.getFont(R.font.ticketing)
        p.textSize = 145F
        canvas.drawText((score + 1000).toString().substring(1),39f, 136f, p)
        gameField.draw(canvas)
        sneck.draw(canvas)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when (event!!.action) {
            MotionEvent.ACTION_DOWN -> {
                touchCoordinate = Coordinate(event.x, event.y)
                return true
            }

            MotionEvent.ACTION_MOVE -> {
                if (touchCoordinate != null)
                    if (distance_max(Coordinate(event.x, event.y), touchCoordinate!!) > 60f) {
                        when {
                            event.y - touchCoordinate!!.y < -60f -> intendedDirection =
                                Direction.UP

                            event.x - touchCoordinate!!.x > 60f -> intendedDirection =
                                Direction.RIGHT

                            event.y - touchCoordinate!!.y > 60f -> intendedDirection =
                                Direction.DOWN

                            event.x - touchCoordinate!!.x < -60f -> intendedDirection =
                                Direction.LEFT
                        }
                        touchCoordinate = null
                    }
                return true
            }

            MotionEvent.ACTION_UP -> {
                touchCoordinate = null
                return true
            }
        }
        return super.onTouchEvent(event)
    }
}