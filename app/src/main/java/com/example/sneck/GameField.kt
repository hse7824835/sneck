package com.example.sneck

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.BitmapFactory.Options
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect

class GameField(context: Context) {
    private val field: Array<Cell> = Array(512) { Cell.NONE }
    private val block: Bitmap
    private val apple: Bitmap
    private val grape: Bitmap
    private val banana: Bitmap
    var hasFood: Boolean = true

    init {
        val opts = BitmapFactory.Options()
        opts.inScaled = false
        block = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.block,
            opts
        )
        apple = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.apple,
            opts
        )
        grape = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.grape,
            opts
        )
        banana = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.banana,
            opts
        )
    }

    fun get(coordinate: IntCoordinate): Cell {
        return field[coordinate.toInt()]
    }

    fun set(coordinate: IntCoordinate, cell: Cell) {
        field[coordinate.toInt()] = cell
    }

    fun spawnBlock() {
        field[(0..511).filter { field[it] == Cell.NONE }.random()] = Cell.BLOCK
    }

    fun spawnFood(sneck: Sneck) {
        val sneckLength = sneck.sneckBody.size - 1
        val gravityVector = when (sneck.gravity) {
            Direction.UP -> IntCoordinate(0, -1)
            Direction.RIGHT -> IntCoordinate(1, 0)
            Direction.DOWN -> IntCoordinate(0, 1)
            Direction.LEFT -> IntCoordinate(-1, 0)
        }
        val steps = arrayListOf(
            IntCoordinate(0, -1),
            IntCoordinate(1, 0),
            IntCoordinate(0, 1),
            IntCoordinate(-1, 0)
        )
        val maxHeights: Array<Int> = Array(512) { -1 }
        val queue: ArrayDeque<IntCoordinate> = ArrayDeque()
        queue.addAll(when (sneck.gravity) {
            Direction.UP -> (0..15).map { IntCoordinate(it, 0) }
            Direction.RIGHT -> (0..31).map { IntCoordinate(15, it) }
            Direction.DOWN -> (0..15).map { IntCoordinate(it, 31) }
            Direction.LEFT -> (0..31).map { IntCoordinate(0, it) }
        }.filter { field[it.toInt()] != Cell.BLOCK })
        while (queue.isNotEmpty()) {
            val cur = queue.first()
            queue.removeFirst()
            val below = cur + gravityVector
            if (below.x > 15 || below.x < 0 || below.y > 31 || below.y < 0) {
                maxHeights[cur.toInt()] = sneckLength
            } else if (field[below.toInt()] == Cell.BLOCK) {
                maxHeights[cur.toInt()] = sneckLength
            }
            val curHeight = maxHeights[cur.toInt()]
            if (curHeight > 0)
                for (step in steps) {
                    val wants = cur + step
                    if (wants.x > 15 || wants.x < 0 || wants.y > 31 || wants.y < 0)
                        continue
                    if (field[wants.toInt()] == Cell.BLOCK)
                        continue
                    if (curHeight > maxHeights[wants.toInt()] + 1) {
                        maxHeights[wants.toInt()] = curHeight - 1
                        queue.addLast(wants)
                    }
                }
        }
        try {
            field[(0..511).filter { maxHeights[it] >= 0 && field[it] != Cell.SNECK && field[it] != Cell.BLOCK }
                .random()] =
                arrayListOf(Cell.APPLE, Cell.GRAPE, Cell.BANANA).random()
            hasFood = true
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun draw(canvas: Canvas) {
        for (i in 0..511) {
            val coordinate = i.toIntCoordinate()
            canvas.drawBitmap(
                when (field[i]) {
                    Cell.BLOCK -> block
                    Cell.APPLE -> apple
                    Cell.GRAPE -> grape
                    Cell.BANANA -> banana
                    else -> continue
                }, Rect(0, 0, 60, 60), Rect(
                    coordinate.x * 60 + 60,
                    coordinate.y * 60 + 180,
                    coordinate.x * 60 + 120,
                    coordinate.y * 60 + 240
                ), Paint()
            )
        }
    }

}
