package com.example.sneck

import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import androidx.activity.ComponentActivity

class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(event == null)
            return super.onTouchEvent(null)
        if(event.action != MotionEvent.ACTION_DOWN)
            return super.onTouchEvent(event)
        if(event.x > 114f && event.x < 966f && event.y > 1194f && event.y < 1446f)
            startActivity(Intent(this, DifficultyActivity::class.java))
        if(event.x > 114f && event.x < 966f && event.y > 1614f && event.y < 1872f)
            startActivity(Intent(this, AboutActivity::class.java))
        return super.onTouchEvent(event)
    }
}
