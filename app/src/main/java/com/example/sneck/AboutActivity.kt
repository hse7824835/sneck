package com.example.sneck

import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.sneck.ui.theme.SneckTheme
import java.io.File

class AboutActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.about_layout)
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if(event == null)
            return super.onTouchEvent(null)
        if(event.action != MotionEvent.ACTION_DOWN)
            return super.onTouchEvent(event)
        if(event.x > 114f && event.x < 966f && event.y > 1854f && event.y < 2112f) {
            File(this.filesDir, "highscores.txt").writeText("000000000")
            startActivity(Intent(this, MainActivity::class.java))
        }

        return super.onTouchEvent(event)
    }
}
