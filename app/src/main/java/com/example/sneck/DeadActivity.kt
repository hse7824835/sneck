package com.example.sneck

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.widget.TextView
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.ui.Modifier
import com.example.sneck.ui.theme.SneckTheme
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader
import java.io.OutputStream
import java.io.OutputStreamWriter
import java.lang.Integer.max

class DeadActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.dead_activity)
        val score = intent.extras!!.getInt("score")
        val range = when (intent.extras!!.getInt("moveTime")) {
            30 -> 0..2
            20 -> 3..5
            15 -> 6..8
            else -> 0..0
        }
        findViewById<TextView>(R.id.score_text_id).text = (score + 1000).toString().substring(1)
        try {
            var highscores =
                File(this.filesDir, "highscores.txt").readLines()[0]
            highscores = highscores.replaceRange(
                range,
                (max(score, highscores.substring(range).toInt()) + 1000).toString().substring(1)
            )
            Log.w("geg", highscores.substring(range).toInt().toString())
            Log.w("geg", score.toString())
            File(this.filesDir, "highscores.txt").writeText(highscores)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event == null)
            return super.onTouchEvent(null)
        if (event.action != MotionEvent.ACTION_DOWN)
            return super.onTouchEvent(event)
        if (event.x < 114f || event.x > 966f)
            return super.onTouchEvent(event)
        if (event.y > 1200f && event.y < 1458f) {
            val i = Intent(this, GameActivity::class.java)
            i.putExtra("moveTime", intent.extras!!.getInt("moveTime"))
            startActivity(i)
        }
        if (event.y > 1662f && event.y < 1920f) {
            startActivity(Intent(this, MainActivity::class.java))
        }
        return super.onTouchEvent(event)
    }
}
