package com.example.sneck

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.widget.TextView
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.annotation.RequiresApi
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.sneck.ui.theme.SneckTheme
import java.io.BufferedReader
import java.io.File
import java.io.InputStreamReader

class DifficultyActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.difficulty_activity)
        try {
            val highscores = try {
                File(this.filesDir, "highscores.txt").readLines()[0]
            } catch (e: Exception) {
                File(this.filesDir, "highscores.txt").writeText("000000000")
                "000000000"
            }
            findViewById<TextView>(R.id.easy_text_id).text =
                "HIGHSCORE: " + highscores.substring(0, 3)
            findViewById<TextView>(R.id.normal_text_id).text =
                "HIGHSCORE: " + highscores.substring(3, 6)
            findViewById<TextView>(R.id.hard_text_id).text =
                "HIGHSCORE: " + highscores.substring(6, 9)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        if (event == null)
            return super.onTouchEvent(null)
        if (event.action != MotionEvent.ACTION_DOWN)
            return super.onTouchEvent(event)
        if (event.x < 114f || event.x > 966f)
            return super.onTouchEvent(event)
        val i = Intent(this, GameActivity::class.java)
        when {
            event.y > 414f && event.y < 662f -> i.putExtra("moveTime", 30)
            event.y > 954f && event.y < 1212f -> i.putExtra("moveTime", 20)
            event.y > 1494f && event.y < 1752f -> i.putExtra("moveTime", 15)
            else -> return super.onTouchEvent(event)
        }
        startActivity(i)
        return super.onTouchEvent(event)
    }
}
