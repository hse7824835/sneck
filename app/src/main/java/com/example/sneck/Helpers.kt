package com.example.sneck

import java.lang.Float.max
import kotlin.math.abs

class Coordinate(var x: Float, var y: Float)

class IntCoordinate(var x: Int, var y: Int) {
}

fun distance_sq(a: Coordinate, b: Coordinate): Float {
    return (a.x - b.x) * (a.x - b.x) + (a.y - b.y) * (a.y - b.y)
}

fun distance_max(a: Coordinate, b: Coordinate): Float {
    return max(abs(a.x - b.x), abs(a.y - b.y))
}

fun Int.toIntCoordinate(): IntCoordinate {
    return IntCoordinate(this % 16, this / 16)
}

fun IntCoordinate.toInt(): Int {
    return this.x + this.y * 16
}

fun IntCoordinate.toCoordinate(): Coordinate {
    return Coordinate(this.x.toFloat(), this.y.toFloat())
}

operator fun IntCoordinate.plus(other: IntCoordinate): IntCoordinate {
    return IntCoordinate(this.x + other.x, this.y + other.y)
}

operator fun IntCoordinate.minus(other: IntCoordinate): IntCoordinate {
    return IntCoordinate(this.x - other.x, this.y - other.y)
}

operator fun IntCoordinate.times(scalar: Int): IntCoordinate {
    return IntCoordinate(this.x * scalar, this.y * scalar)
}

operator fun Coordinate.plus(other: Coordinate): Coordinate {
    return Coordinate(this.x + other.x, this.y + other.y)
}

operator fun Coordinate.times(scalar: Float): Coordinate {
    return Coordinate(this.x * scalar, this.y * scalar)
}

enum class Direction { UP, RIGHT, DOWN, LEFT }

enum class Cell { BLOCK, APPLE, GRAPE, BANANA, SNECK, NONE }