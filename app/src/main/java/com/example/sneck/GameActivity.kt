package com.example.sneck

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.addCallback


class GameActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(Game(this, intent.extras!!.getInt("moveTime")))
    }

    init {
        onBackPressedDispatcher.addCallback(this) {}
    }
}
