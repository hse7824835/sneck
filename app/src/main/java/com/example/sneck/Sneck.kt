package com.example.sneck

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Rect
import android.graphics.RectF
import android.util.Log
import kotlin.math.sqrt

class Sneck(context: Context, private val gameField: GameField) {
    val sneckBody: ArrayDeque<IntCoordinate> = ArrayDeque()
    var fallUntil: Long = Long.MIN_VALUE
    private var fallingSince: Long = Long.MIN_VALUE
    private var fallAmount: Int = 0
    var gravity: Direction = Direction.DOWN
    private val gravitationalAcceleration: Float = 100f
    private val bitmap: Bitmap

    init {
        sneckBody.addFirst(IntCoordinate(5, 31))
        sneckBody.addFirst(IntCoordinate(5, 30))
        gameField.set(IntCoordinate(5, 31), Cell.SNECK)
        gameField.set(IntCoordinate(5, 30), Cell.SNECK)
        gameField.set(IntCoordinate(9, 29), Cell.APPLE)
        val opts = BitmapFactory.Options()
        opts.inScaled = false
        bitmap = BitmapFactory.decodeResource(
            context.resources,
            R.drawable.sneck_spritesheet,
            opts
        )
    }

    fun draw(canvas: Canvas) {
        val timestamp: Long = System.currentTimeMillis()
        if (fallUntil > timestamp) {
            val gravityVector = when (gravity) {
                Direction.UP -> Coordinate(0f, -1f)
                Direction.RIGHT -> Coordinate(1f, 0f)
                Direction.DOWN -> Coordinate(0f, 1f)
                Direction.LEFT -> Coordinate(-1f, 0f)
            }
            sneckBody.addFirst(sneckBody.first())
            sneckBody.addLast(sneckBody.last())
            for (i in 1 until sneckBody.size - 1) {
                val spriteCoordinate =
                    (((sneckBody[i - 1] - sneckBody[i]) + (sneckBody[i] - sneckBody[i + 1]) * 3) + IntCoordinate(
                        4,
                        4
                    )) * 60
                val segmentCoordinate =
                    sneckBody[i].toCoordinate() + gravityVector * (gravitationalAcceleration * ((timestamp - fallingSince).toFloat() * .001f * ((timestamp - fallingSince).toFloat() * .001f)) * .5f - fallAmount.toFloat())
                canvas.drawBitmap(
                    bitmap,
                    Rect(
                        spriteCoordinate.x,
                        spriteCoordinate.y,
                        spriteCoordinate.x + 60,
                        spriteCoordinate.y + 60
                    ),
                    RectF(
                        segmentCoordinate.x * 60f + 60f,
                        segmentCoordinate.y * 60f + 180f,
                        segmentCoordinate.x * 60f + 120f,
                        segmentCoordinate.y * 60f + 240f
                    ),
                    Paint()
                )
            }
            sneckBody.removeFirst()
            sneckBody.removeLast()
        } else {
            sneckBody.addFirst(sneckBody.first())
            sneckBody.addLast(sneckBody.last())
            for (i in 1 until sneckBody.size - 1) {
                val spriteCoordinate =
                    (((sneckBody[i - 1] - sneckBody[i]) + (sneckBody[i] - sneckBody[i + 1]) * 3) + IntCoordinate(
                        4,
                        4
                    )) * 60
                canvas.drawBitmap(
                    bitmap,
                    Rect(
                        spriteCoordinate.x,
                        spriteCoordinate.y,
                        spriteCoordinate.x + 60,
                        spriteCoordinate.y + 60
                    ),
                    Rect(
                        sneckBody[i].x * 60 + 60,
                        sneckBody[i].y * 60 + 180,
                        sneckBody[i].x * 60 + 120,
                        sneckBody[i].y * 60 + 240
                    ),
                    Paint()
                )
            }
            sneckBody.removeFirst()
            sneckBody.removeLast()
        }
    }

    fun move(intendedDirection: Direction) {
        if (fallUntil > System.currentTimeMillis())
            return
        val wants = sneckBody.first() + when (intendedDirection) {
            Direction.UP -> IntCoordinate(0, -1)
            Direction.RIGHT -> IntCoordinate(1, 0)
            Direction.DOWN -> IntCoordinate(0, 1)
            Direction.LEFT -> IntCoordinate(-1, 0)
        }
        if (wants.x > 15 || wants.x < 0 || wants.y > 31 || wants.y < 0)
            return
        when (gameField.get(wants)) {
            Cell.BLOCK -> return
            Cell.SNECK -> return
            Cell.NONE -> {
                gameField.set(sneckBody.last(), Cell.NONE)
                sneckBody.removeLast()
            }

            Cell.APPLE -> gameField.hasFood = false
            Cell.GRAPE -> {
                val dirs = hashSetOf(Direction.UP, Direction.RIGHT, Direction.DOWN, Direction.LEFT)
                dirs.remove(gravity)
                gravity = dirs.random()
                gameField.hasFood = false
            }

            Cell.BANANA -> {
                gameField.spawnBlock()
                gameField.hasFood = false
            }
        }
        gameField.set(wants, Cell.SNECK)
        sneckBody.addFirst(wants)
        this.fall()
    }

    private fun fall() {
        fallAmount = 0
        val gravityVector = when (gravity) {
            Direction.UP -> IntCoordinate(0, -1)
            Direction.RIGHT -> IntCoordinate(1, 0)
            Direction.DOWN -> IntCoordinate(0, 1)
            Direction.LEFT -> IntCoordinate(-1, 0)
        }
        var falling = true
        while (falling) {
            ++fallAmount
            for (segment in sneckBody) {
                val wants = segment + gravityVector * fallAmount
                if (wants.x > 15 || wants.x < 0 || wants.y > 31 || wants.y < 0) {
                    falling = false
                    break
                }
                if (gameField.get(wants) != Cell.NONE && gameField.get(wants) != Cell.SNECK) {
                    falling = false
                    break
                }
            }
        }
        --fallAmount
        for (i in 0 until sneckBody.size) {
            gameField.set(sneckBody[i], Cell.NONE)
            sneckBody[i] += gravityVector * fallAmount
        }
        for (i in sneckBody)
            gameField.set(i, Cell.SNECK)
        fallingSince = System.currentTimeMillis()
        fallUntil =
            fallingSince + (sqrt(2f * fallAmount.toFloat() / gravitationalAcceleration) * 1000f).toLong()
    }
}
